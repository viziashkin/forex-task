import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ForexWallItemComponent } from './forex-wall-item.component';

describe('ForexWallDisplayComponent', () => {
  let component: ForexWallItemComponent;
  let fixture: ComponentFixture<ForexWallItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ForexWallItemComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ForexWallItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
