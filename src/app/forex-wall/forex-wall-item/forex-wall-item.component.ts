import {
  ICurrencyItem,
  ICurrencyData,
  intervalMap,
} from './../forex-wall.model';
import { ForexWallService } from './../forex-wall.service';
import {
  Component,
  EventEmitter,
  HostBinding,
  HostListener,
  Input,
  OnInit,
  Output,
} from '@angular/core';

@Component({
  selector: 'app-forex-wall-item',
  templateUrl: './forex-wall-item.component.html',
  styleUrls: ['./forex-wall-item.component.scss'],
})
export class ForexWallItemComponent implements OnInit {
  @Input() currencyItem: ICurrencyItem;

  @Output() deleteSelf: EventEmitter<ICurrencyItem> = new EventEmitter();

  displayData: ICurrencyData;
  intervalRef: any;
  private defaultInterval = 5000;
  intervalMap = intervalMap;
  objectkeys = Object.keys;

  @HostBinding('class.wall-item') get getDefaultClass(): boolean {
    return true;
  }

  constructor(private wallService: ForexWallService) {}

  getData(): void {
    this.wallService
      .retrieveCurrencyData(this.currencyItem.currencyPair)
      .then((result) => {
        this.displayData = result;
      });
  }

  setUpdateInterval(): void {
    clearInterval(this.intervalRef);
    this.intervalRef = setInterval(() => {
      this.getData();
    }, this.currencyItem.interval || this.defaultInterval);
  }

  onIntervalChange(event: any): void {
    const value = event.target.value;
    let intervalToSet = intervalMap[value];

    this.currencyItem.interval = intervalToSet;
    this.setUpdateInterval();
  }

  onRemoveClick(): void {
    this.deleteSelf.emit(this.currencyItem);
  }

  ngOnInit(): void {
    this.getData();
    this.setUpdateInterval();
  }

  ngOnDestroy(): void {
    clearInterval(this.intervalRef);
  }
}
