import { TestBed } from '@angular/core/testing';

import { ForexWallStoreService } from './forex-wall-store.service';

describe('ForexWallStoreService', () => {
  let service: ForexWallStoreService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ForexWallStoreService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
