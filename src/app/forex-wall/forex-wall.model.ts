export interface ICurrencyPair {
  a: string;
  b: string;
}

export interface ICurrencyItem {
  currencyPair: ICurrencyPair;
  interval: number;
}

export interface ICurrencyData {
  ticker: string;
  bid: string;
  ask: string;
  open: string;
  low: string;
  high: string;
  changes: number;
  date: string;
}

export const intervalMap = {
  '3s': 3000,
  '5s': 5000,
  '10s': 10000,
  '30s': 30000,
};
