import { ForexWallStoreService } from './forex-wall-store.service';
import { ICurrencyItem, ICurrencyPair } from './forex-wall.model';
import { ForexWallService } from './forex-wall.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-forex-wall',
  templateUrl: './forex-wall.component.html',
  styleUrls: ['./forex-wall.component.scss'],
})
export class ForexWallComponent implements OnInit {
  currencyItems: ICurrencyItem[] = [];

  constructor(
    private wallService: ForexWallService,
    private store: ForexWallStoreService
  ) {}

  onSubmit(currencyItem: ICurrencyItem): void {
    this.store.addCurrencyItem(currencyItem);
  }

  handleDeleteItem(currencyItem: ICurrencyItem) {
    this.store.removeCurrencyItem(currencyItem);
  }

  ngOnInit(): void {
    this.store.currencyItems$.subscribe((items) => {
      this.currencyItems = items;
    });
  }
}
