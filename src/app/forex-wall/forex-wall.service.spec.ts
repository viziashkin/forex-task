import { TestBed } from '@angular/core/testing';

import { ForexWallService } from './forex-wall.service';

describe('ForexWallService', () => {
  let service: ForexWallService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ForexWallService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
