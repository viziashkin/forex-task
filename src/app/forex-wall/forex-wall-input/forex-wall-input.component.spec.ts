import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ForexWallInputComponent } from './forex-wall-input.component';

describe('ForexWallInputComponent', () => {
  let component: ForexWallInputComponent;
  let fixture: ComponentFixture<ForexWallInputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ForexWallInputComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ForexWallInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
