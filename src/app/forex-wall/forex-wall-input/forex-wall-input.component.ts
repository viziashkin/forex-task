import { ForexWallStoreService } from './../forex-wall-store.service';
import {
  ICurrencyItem,
  ICurrencyPair,
  intervalMap,
} from './../forex-wall.model';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-forex-wall-input',
  templateUrl: './forex-wall-input.component.html',
  styleUrls: ['./forex-wall-input.component.scss'],
})
export class ForexWallInputComponent implements OnInit {
  form: FormGroup;
  currencyOptions: ICurrencyPair[];
  intervalMap = intervalMap;
  objectkeys = Object.keys;

  @Output() notifySubmit: EventEmitter<ICurrencyItem> = new EventEmitter();

  constructor(private store: ForexWallStoreService) {
    this.form = new FormGroup({
      query: new FormControl('', Validators.required),
      interval: new FormControl('3s'),
    });
  }

  onBlur() {
    const value = this.form.get('query').value;
    const rawCurrCP = value && this.form.get('query').value.split('/');

    if (!rawCurrCP || rawCurrCP.lenght < 2) {
      this.form.get('query').setErrors({ invalid: true });
      return;
    }
    const currCP = {
      a: rawCurrCP[0],
      b: rawCurrCP[1],
    };

    const isValid = !!this.currencyOptions.find((element: ICurrencyPair) => {
      return element.a === currCP.a && element.b === currCP.b;
    });

    this.form.get('query').setErrors(isValid ? null : { invalid: isValid });
  }

  onSubmit(): void {
    const queryControl = this.form.get('query');

    if (!this.form.valid) {
      queryControl.markAsDirty()
      return;
    }
    const queryVal = queryControl.value;
    if (!queryVal) {
      return;
    }
    const intervalControl = this.form.get('interval');
    const intervalVal = intervalControl.value;
    let interval = intervalMap[intervalVal];

    const rawPair = queryVal.split('/');
    const currencyPair = {
      a: rawPair[0],
      b: rawPair[1],
    };
    const currencyItem: ICurrencyItem = {
      currencyPair: currencyPair,
      interval: interval,
    };
    this.notifySubmit.emit(currencyItem);
    queryControl.setValue('');
    queryControl.markAsPristine();
  }

  ngOnInit(): void {
    this.store.relevantCurrencyList$.subscribe((list) => {
      this.currencyOptions = list;
    });
  }
}
