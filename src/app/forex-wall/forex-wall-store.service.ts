import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ICurrencyItem, ICurrencyPair } from './forex-wall.model';
import { mergeMap, map } from 'rxjs/operators';
import { combineLatest } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ForexWallStoreService {
  private readonly _currencyList = new BehaviorSubject<any>([]);
  readonly currencyList$ = this._currencyList.asObservable();

  private get currencyList(): any {
    return this._currencyList.getValue();
  }

  private set currencyList(val: any) {
    this._currencyList.next(val);
  }

  private _currencyItems = new BehaviorSubject<ICurrencyItem[]>([]);
  readonly currencyItems$ = this._currencyItems.asObservable();

  private get currencyItems(): ICurrencyItem[] {
    return this._currencyItems.getValue();
  }

  private set currencyItems(val: ICurrencyItem[]) {
    this._currencyItems.next(val);
  }

  relevantCurrencyList$ = combineLatest([
    this.currencyList$,
    this.currencyItems$,
  ]).pipe(
    map((result) => {
      return this.getRelevantCurrencyList(result[0], result[1]);
    })
  );

  getRelevantCurrencyList(
    currencyList: ICurrencyPair[],
    currencyItems: ICurrencyItem[]
  ) {
    const currencyPairsToRemove = currencyItems.map(
      (item) => item.currencyPair
    );

    return currencyList.filter(comparer(currencyPairsToRemove));

    function comparer(otherArray) {
      return function (current) {
        return (
          otherArray.filter(function (other) {
            return other.a == current.a && other.b == current.b;
          }).length == 0
        );
      };
    }
  }

  updateCurrencyList(value: any) {
    this.currencyList = value;
  }

  addCurrencyItem(item: ICurrencyItem) {
    if (!this.findCurrencyItem(item)) {
      this.currencyItems = [...this.currencyItems, item];
    }
  }

  findCurrencyItem(item: ICurrencyItem): ICurrencyItem {
    return this.currencyItems.find((element) => {
      if (this.compareCurrencyItems(item, element)) {
        return element;
      } else {
        return false;
      }
    });
  }

  removeCurrencyItem(item: ICurrencyItem) {
    this.currencyItems = this.currencyItems.filter((element: ICurrencyItem) => {
      return !this.compareCurrencyItems(item, element);
    });
  }

  compareCurrencyItems(item1: ICurrencyItem, item2: ICurrencyItem): boolean {
    return (
      item1.currencyPair.a === item2.currencyPair.a &&
      item1.currencyPair.b === item2.currencyPair.b
    );
  }
}
