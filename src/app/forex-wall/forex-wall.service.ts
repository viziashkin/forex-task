import { ForexWallStoreService } from './forex-wall-store.service';
import { ICurrencyPair } from './forex-wall.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ForexWallService {
  config: any;

  constructor(private http: HttpClient, private store: ForexWallStoreService) {
    this.loadConfig();
  }

  loadConfig(): void {
    this.http.get('assets/config.json').subscribe((result) => {
      this.config = result;
      this.getCurrencyPairsList();
    });
  }

  getCurrencyPairsList() {
    this.retrieveForexList().then((result) => {
      const currencyList = [];
      result.forEach((element) => {
        const currencies = element.ticker.split('/');
        const currencyPair: ICurrencyPair = {
          a: currencies[0],
          b: currencies[1],
        };
        currencyList.push(currencyPair);
      });
      this.store.updateCurrencyList(currencyList);
    });
  }

  retrieveForexList(): Promise<any> {
    if (this.config.useMockedData) {
      return new Promise((resolve) => {
        resolve(this.config['sampleAllForex']['forexList']);
      });
    } else {
      return this.http
        .get(`${this.config.currencyListUrl}?apikey=${this.config.apiKey}`)
        .toPromise();
    }
  }

  retrieveCurrencyData(currencyPair: ICurrencyPair): Promise<any> {
    if (this.config.useMockedData) {
      return new Promise((resolve) => {
        resolve(this.config['sampleSinglePair'][0]);
      });
    } else {
      return this.http
        .get(
          `${this.config.currencyDataUrl}${currencyPair.a}${currencyPair.b}?apikey=${this.config.apiKey}`
        )
        .pipe(map((result) => result[0]))
        .toPromise();
    }
  }
}
