import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ForexWallComponent } from './forex-wall.component';

describe('ForexWallComponent', () => {
  let component: ForexWallComponent;
  let fixture: ComponentFixture<ForexWallComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ForexWallComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ForexWallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
