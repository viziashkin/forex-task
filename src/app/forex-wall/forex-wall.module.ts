import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ForexWallRoutingModule } from './forex-wall-routing.module';
import { ForexWallComponent } from './forex-wall.component';
import { ForexWallItemComponent } from './forex-wall-item/forex-wall-item.component';
import { ForexWallInputComponent } from './forex-wall-input/forex-wall-input.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UiComponentsModule } from '../ui-components/ui-components.module';

@NgModule({
  declarations: [
    ForexWallComponent,
    ForexWallItemComponent,
    ForexWallInputComponent,
  ],
  imports: [
    CommonModule,
    ForexWallRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    UiComponentsModule,
  ],
})
export class ForexWallModule {}
