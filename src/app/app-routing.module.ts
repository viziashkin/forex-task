import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'forex-wall',
    loadChildren: () =>
      import('./forex-wall/forex-wall.module').then((m) => m.ForexWallModule),
  },
  {
    path: '**',
    redirectTo: 'forex-wall',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
