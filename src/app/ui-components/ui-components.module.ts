import { IconModule } from './icon/icon.module';
import { ButtonModule } from './button/button.module';
import { FormFieldModule } from './form-field/form-field.module';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [CommonModule, FormFieldModule, ButtonModule, IconModule],
  exports: [FormFieldModule, ButtonModule, IconModule],
})
export class UiComponentsModule {}
