# ForexTask

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

Please remember that there is a limit on requests per day. Don't select low value for interval or set `useMockedData: true` in `src\assets\config.json`. 

## Description
Apart of requirements, I've developed it to handle multiple currencies at a time with update interval configuration per item. As well, on load a list of all available currency pairs is being loaded and then suggested in currency pair autocomplete. The field is validated and it is not possible to submit invalid value (at least I suppose so).

Somewhat responsive

Some sugar like running prettier on pre-commit via husky.

## Structure

Added useless for this case routing, main module is lazy-loaded.

### Shared components

Set of shared componens library-alike, bundled in a single module (`src\app\ui-components`)

1. **Button** - just a directive to apply default class and remove focus on click
2. **Form-field** - custom form-field to handle label, error message, icon
3. **Icon** - a simple component that accepts icon name and takes an svg from `src\assets\symbol-defs.svg`
4. **Theme** variables - some colors and global styles, mixins, breakpoints (`src\app\theme\variables.scss`)

## Components
1. *forex-wall-component* - main component, puts any new currency item into the store or deletes it. 
2. *forex-wall-input* - a form that accepts a list of currencyPairs for autocomplete, performs basic validation and submits data to the parent. And yes, it doesn't suggest to add currency pairs that are already added. 
3. *forex-wall-item* - a components represents each currency pair exchange data, contains dropdown for interval selection, responsible for calling API for an update each polling tick. Has some styling (change field will be green on positive and red on negative).

### Store

A service implementing a store for main data handling vie BeehavioralSubject, as well as some modigications of currency suggestions list.
(`src\app\forex-wall\forex-wall-store.service.ts`)

### Service
*forex-wall.service* - since the app is quite small and there are only 2 endpoints, there is no separate service for handling API calls onl, but that definitely would be a case for an app with more APIs. The service is responsible for retrieving data from forex server and from local config that contains some static sample data (since limited number of requests per day).
